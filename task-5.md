# Задание 5

```text
ОПИСАНИЕ

Имеется страница авторизации.  Для входа используется учетная запись от стороннего сервиса. Web-часть реализована как SPA-приложение.


ПРИМЕРЫ

Сервер: DemoServer, Логин: 20001, Пароль: Легкий Пароль
Сервер: RealServer, Логин: 45679, Пароль: Сложный Пароль


ЗАДАЧА
Требуется проверить продукт на наличие уязвимостей. 
Перечислите  шаги для поиска проблем (любые другие действия, которые требуется сделать)
```


---

## Ответ

### Проверка продукта на наличии уязвимостей

Необходимо выполнить следующее:

1. ##### проверить экранирование спецсимволов в запросе

2. ##### проверить, чтобы все конфиденциальные cookie были с флагом `httpOnly` и `secure`

> Cookie с флагом “secure” передаются на сервер только по протоколу HTTPS.
> 
> Cookie с флагом “httpOnly” защищены от манипуляции JavaScript через документ, где хранятся cookie.

![http-only-check](./images/http-only-check.png)

3. ##### проверить, не хранятся ли пароли от сайта в cookie

4. ##### проверить, что установлен лимит на попытки ввода пароля
Это усложнит задачу злоумышленнику при попытке подбора паролей

5. ##### проверить, что пароли пользователей храняться в БД в зашифрованном виде
Это усложнит задачу злоумышленнику при получении доступа к таблицам с именами пользователей и паролями через SQL-инекции

6. ##### проверить, что производится редирект с `http://` на `https://`
Попробовать удалить символ `s` после http и если редиректа не произойдет то есть риск стать жертвой сниффинга.
Злоумышленник может перехватить WiFi трафик.

---


