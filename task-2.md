# Задание 2

```text
ОПИСАНИЕ

Имеется профиль трейдера на котором отображаются показатели (графики) прибыльности за весь период торговли. 
На данный момент система считает и отображает данные верно.


ПРОБЛЕМА

Требуется выпустить новую версию с оптимизацией скорости расчетов, которая может поломать расчеты или отображение


УСЛОВИЯ

Во время ручного тестирования достаточно сложно проверить правильность расчетов (требуется пересчитать большой объем данных) и верную отрисовку графиков.
Выполнять ручное тестирование каждый раз занимает большое кол-во времени
Сервис имеет API для получения показателей по профилю
Сервис заранее рассчитывает показатели и хранит их в БД
Данные для расчета берутся из различных сторонних сервисов


ЗАДАЧА

Распишите какие требуется выполнить шаги, для того, чтобы безопасно выпустить новую в убедиться что расчеты не сломались после оптимизации. 
Опишите принцип проверки (как они будут проверять показатели, как будут работать с данными)
```


---

## Ответ


### Во-первых
Для новой версии от `develop` создается новая отдельная ветка, 
в которой будут все изменения направленные на оптимизацию скорости расчетов, например: 
`feature/profit-calc-optimization`

---
### Далее
Когда разработка была завершена, я локально переключаюсь на ветку `feature/profit-calc-optimization` и провожу
тестирование обновленного функционала

---
### Мне необходимо
1. проверить корректность рассчетов
2. измерить скорость расчетов и сравнить со старой версией


#### Проверка рассчетов:
1. сделать выборку в БД с помощью SQL запроса - и получить фактический результат

Например, получить показатели для пользователя с `user_id` = `1`, из таблицы  `profitability`:
```
SELECT * FROM profitability
Where user_id = '1'
```
2. использя формулы расчета, описанные в документации <br>(P.S. такие формулы для расчетов я обычно получал от бизнес-аналитика),
и они прописывались в документации к продукту.

Затем взяв рассчетные данные из сторонник сервисов я произведу расчет и сверю с данными из таблицы `profitability`

- Если данные совпадают -  **значит расчет верный!**
- Если данные не совпадают - напишу комментарий в задаче в JIRA какой из параметров не правильно рассчитался.

3. Допустим расчеты производятся правильно, затем я проверю метод `GET`, который получает показатели по конкретному пользователю
   (в Swagger или Postman) и удостоверюсь, что метод возвращает те-же данные которые были получены в `п.1`

**ЗДЕСЬ Я ВЫПОЛНИЛ ПЕРВУЮ ЗАДАЧУ - проверить корректность рассчетов**

4. далее проверю как быстро был получен ответ

![response-time](./images/response-time.png)

- проверю, соответствует ли он требованию описанному в задаче
- проверю, стал ли ответ приходить быстрее

#### Автоматизация
Чтобы не производить расчет в ручную, для него можно написать **автотест**, который будет делать следующее:

1. получаем данные со сторонних сервисов, с помощью GET запросов (сохраняем в переменные)
<br>Для этих целей я использую Cypress, а именно встроенные метод `cy.request('path')`
   https://docs.cypress.io/api/commands/request#URL
<br><br>

3. производим расчет по заданным формулам (сохраняем в переменные)
4. получаем данные по показателям для выбранного пользователя (так-же с помощью `cy.request()`)
5. сравниваем то что мы насчитали с тем что получено из нашей БД
6. если данные соответствуют - значит тест **PASSED!**
