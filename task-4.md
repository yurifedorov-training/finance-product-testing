# Задание 4

```text
ЗАДАЧА

Требуется реализовать метод по расчету геометрического среднего (С#). 
Реализуйте юнит-тесты для проверки данного метода. 
Важно предусмотреть исключительные ситуации при передаче параметров.


ЦЕЛЬ

Проверить знания C#
```


---

## Ответ

К сожалению на `C#` не писал ни-разу.

---

> ГЕОМЕТРИЧЕСКОЕ СРЕДНЕЕ, среднее геометрическое чисел n - это n-ый корень произведений данных чисел. 
> 
> Например, квадратный корень из произведений двух чисел 8 и 2 есть среднее геометрическое 8 и 2, равное Ц(832)=4.

Могу предложить простое решение на JS, с импользованием метода [Math.sqrt()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/sqrt)

например это файл `geometric-аverage.js`

```js
function calcGeometricAverage(a, b) {
  return (Math.sqrt(a * b));
}

console.log(calcGeometricAverage(8, 2));// expected output: 4
```

Я имел опыт написания юнит-тестов с использованием фреймворка [Jest](https://jestjs.io/), вот пример:
```js
import { calcGeometricAverage } from './geometric-аverage' // import function

describe('Calculation of the geometric average', () => {
    it('should return 4 for values 8 and 2', function() {
        const result = calcGeometricAverage(8, 2)
        expect(result).toBe(4) // Should be passed :)
    });

    it('should return 4 for values 2 and 8', function() {
        const result = calcGeometricAverage(2, 8)
        expect(result).toBe(4) // Should be passed :)
    });

    it('should NOT return 4 for values 2 and 4', function() {
        const result = calcGeometricAverage(2, 4)
        expect(result).not.toBe(4); // Should be passed :)
    });
})
```

